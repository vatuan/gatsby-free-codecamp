module.exports = {
  siteMetadata: {
    title: "I Love React",
    description: "Project created to learn about gatsbyjs",
    author: "Tuandev98",
  },
  /* Your site config here */

  plugins: [
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.CONTENTFUL_SPACE_ID,
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
      },
    },
    "gatsby-plugin-sharp",
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },

    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          "gatsby-remark-relative-images",
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 1170,
              linkImagesToOriginal: false,
            },
          },
        ],
      },
    },
    "gatsby-plugin-react-helmet",
  ],
}
