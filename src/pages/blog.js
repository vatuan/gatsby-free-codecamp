import React from "react"
import Layout from "../components/layout"
import { graphql, Link, useStaticQuery } from "gatsby"
import blogStyles from "../styles/blog.module.scss"
import Head from "../components/head"

function BlogPage() {
  const data = useStaticQuery(graphql`
    query {
      allContentfulBlogPost(
        filter: { node_locale: { eq: "en-US" } }
        sort: { fields: publishedDate, order: ASC }
      ) {
        edges {
          node {
            title
            slug
            publishedDate(formatString: "MMMM Do, YYYY")
          }
        }
      }
    }
  `)
  console.log({ data })
  return (
    <div className="blog-page">
      <Layout>
        <Head title="Blog" />
        <h1>Blog page</h1>
        <ol className={blogStyles.posts}>
          {data.allContentfulBlogPost.edges.map((edge, index) => {
            return (
              <li key={index} className={blogStyles.post}>
                <Link to={`/blog/${edge.node.slug}`}>
                  {" "}
                  <h2 className={blogStyles.postTitle}>{edge.node.title}</h2>
                  <p className={blogStyles.postPublishedDate}>
                    {edge.node.publishedDate}
                  </p>
                </Link>
              </li>
            )
          })}
        </ol>
        <h2>This is blog page</h2>
      </Layout>
    </div>
  )
}

export default BlogPage
