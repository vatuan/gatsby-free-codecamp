import React from "react"
import Head from "../components/head"
import Layout from "../components/layout"

function AboutPage() {
  return (
    <div className="about-page">
      <Layout>
        <Head title="About" />
        <h1>About Page</h1>
      </Layout>
    </div>
  )
}

export default AboutPage
