import React from "react"
import Head from "../components/head"
import Layout from "../components/layout"

function ContactPage() {
  return (
    <div className="contact">
      <Layout>
        <Head title="Contact" />
        <h1>Contact page</h1>
        <h3>Call me : 0356895599</h3>
      </Layout>
    </div>
  )
}

export default ContactPage
