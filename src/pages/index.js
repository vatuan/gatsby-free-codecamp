import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Head from "../components/head"

export default function Home() {
  return (
    <div className="home">
      <Layout>
        <Head title="Home" />
        <h1>Hello, my name is Vũ Anh Tuấn</h1>
        <h3>Nedd help</h3>
        <Link to="/contact">Contact me</Link>
      </Layout>
    </div>
  )
}
