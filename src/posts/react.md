---
title: "React"
date: "02/04/2020"
---

In this post you'll learn React

```php
import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"

export default function Home() {
  window.document.title = "Home"
  return (
    <div className="home">
      <Layout>
        <h1>Hello, my name is Vũ Anh Tuấn</h1>
        <h3>Nedd help</h3>
        <Link to="/contact">Contact me</Link>
      </Layout>
    </div>
  )
}
```
