import { Link, graphql, useStaticQuery } from "gatsby"
import React from "react"
import styleHeaders from "../styles/header.module.scss"

function Header() {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          description
          author
        }
      }
    }
  `)
  return (
    <header className={styleHeaders.header}>
      <h1>
        <Link className={styleHeaders.title} to="/">
          {data.site.siteMetadata.title}
        </Link>
      </h1>
      <nav>
        <ul className={styleHeaders.navList}>
          <li>
            <Link
              to="/"
              className={styleHeaders.navItem}
              activeClassName={styleHeaders.aciveNavItem}
            >
              Home
            </Link>
          </li>
          <li>
            {" "}
            <Link
              className={styleHeaders.navItem}
              activeClassName={styleHeaders.aciveNavItem}
              to="/blog"
            >
              Blog
            </Link>
          </li>
          <li>
            {" "}
            <Link
              className={styleHeaders.navItem}
              activeClassName={styleHeaders.aciveNavItem}
              to="/about"
            >
              About
            </Link>
          </li>
          <li>
            {" "}
            <Link
              className={styleHeaders.navItem}
              activeClassName={styleHeaders.aciveNavItem}
              to="/contact"
            >
              Contact
            </Link>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header
