import React from "react"
import footerStyles from './footer.module.scss'

function Footer() {
  return (
    <div className={footerStyles.footer}>
      <p>Created by Anh Tuan, © {new Date().getFullYear()}</p>
    </div>
  )
}

export default Footer
