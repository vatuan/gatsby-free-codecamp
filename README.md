# Cai dat

- `npm install -g gatsby-cli`
- `gatsby new ten-project https://github.com/gatsbyjs/gatsby-starter-hello-world`

## 1. Working with gastby pages

- thêm file mới vào folder pages ,
- ví dụ: tạo thêm file about.js thì trên URL : http://localhost:8000/about sẽ hiển thì ra trang about mà không cần dùng đến route

## 2. Linking between pages with gatsby

- sử dụng thẻ `Link` để chuyển giữa các trang, không phải load lại trang giống thẻ `a`

## 3. Creating shared page components

- tạo folder components để dùng chung , tạo components Footer và Header để dùng chung
- sử dụng :

```php
  return (
    <div className="contact">
      <Header />
      <h1>Contact page</h1>
      <h3>Call me : 0356895599</h3>
      <Footer />
    </div>
  )
```

## 4. Creating gatsby page layouts

- tạo file `layout.js` dùng chung cho toàn bộ site

```php
import React from "react"
import Footer from "./footer"
import Header from "./header"

function Layout(props) {
  return (
    <div>
      <Header />
      {props.children}
      <Footer />
    </div>
  )
}

export default Layout
```

- ví dụ khi dùng :

```php
import { Link } from "gatsby"
import React from "react"

import Layout from "../components/layout"

function ContactPage() {
  window.document.title = "Contact"
  return (
    <div className="contact">
      <Layout>
        <h1>Contact page</h1>
        <h3>Call me : 0356895599</h3>
      </Layout>
    </div>
  )
}

export default ContactPage

```

## 5. Styling gatsby projects

- cài đặt : `npm install --save node-sass gatsby-plugin-sass`
- sử dụng :

```php
module.exports = {
  /* Your site config here */
  plugins: [`gatsby-plugin-sass`],
}
```

- rồi import sử dụng như bình thường.

## 6. Styling gatsby with css modules

- mục đích là để tạo ra các locally styles, không xảy ra việc bị trùng tên class trong ứng dụng
- tạo 1 file ví dụ : `header.module.scss`
- sử dụng :

```php
import styleHeaders from "../styles/header.module.scss"

 <header className={styleHeaders.header}>
     // something code here
 </header>
```

- file `header.module.scss`

```php
.header {
  padding: 1rem 0 3rem;
}
```

## 7. Gatsby data with GrapQL

- tạo data demo tại file `gatsby-config.js`

```php
module.exports = {
  siteMetadata: {
    title: "I Love React",
    description: "Project created to learn about gatsbyjs",
    author: "Tuandev98",
  },
  /* Your site config here */

  plugins: [`gatsby-plugin-sass`],
}
```

- dùng GraphQL để lấy dữ liệu demo từ file `gatsby-config.js`
- tại `header.js`

```php
import { Link, graphql, useStaticQuery } from "gatsby"


function Header() {
  // sử  dungj GraphQL :
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          description
          author
        }
      }
    }
  `)
  return (
    <header className={styleHeaders.header}>
      <h1>
        <Link className={styleHeaders.title} to="/">
          {data.site.siteMetadata.title}  // sử  dụng như 1 object thông thường
        </Link>
      </h1>

      //... st code

  )
```

## 8. GraphQL Playground

> > Cho theme GraphQL trông đẹp hơn !

- cài đặt : `npm i env-cmd`
- tại file : `.env.development`

```php
GATSBY_GRAPHQL_IDE = playground
```

- tại file package.json :

```php
  "build": "gatsby build",
  "develop": "env-cmd --file .env.development --fallback gatsby develop",    // thêm một số  cái mới vào
  "format": "prettier --write \"**/*.{js,jsx,ts,tsx,json,md}\"",
```

## 9. Sourcing content from file system

- cài đặt : `npm install --save gatsby-source-filesystem`
- tại file `gatsby-config.js` :

```php
plugins: [
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-source-filesystem`,      // thêm vào để  sử  dụng
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
  ],
```

## 10. Working with markdown posts

- tạo 1 folder `posts` , add 2 file `gatsby.md` và `react.md` vào folder
- để sử dụng được markdown phải cài plugin : `npm install --save gatsby-transformer-remark`
- tại file `gatsby-config.js` :

```php

 // Thêm đoạn này vào
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        // CommonMark mode (default: true)
        commonmark: true,
        // Footnotes mode (default: true)
        footnotes: true,
        // Pedantic mode (default: true)
        pedantic: true,
        // GitHub Flavored Markdown mode (default: true)
        gfm: true,
        // Plugins configs
        plugins: [],
      },
    },
```

- Tại file `blog.js` :

```php
import React from "react"
import Layout from "../components/layout"
import { graphql, useStaticQuery } from "gatsby"

function BlogPage() {
  window.document.title = "Blog"

  // Sử  dụng grapQl để  lấy  data từ file markdowm là gatsby.md và react.md trong ví dụ
  // lúc naỳ data sẽ  là 1 object , và sẽ sử  dụng nó như thường
  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              date
            }
          }
        }
      }
    }
  `)

  console.log({ data })
  return (
    <div className="blog-page">
      <Layout>
        {" "}
        <h1>Blog page</h1>
        <ol>
          {data.allMarkdownRemark.edges.map((edge, index) => {
            return (
              <li key={index}>
                <h2>{edge.node.frontmatter.title}</h2>
                <p>{edge.node.frontmatter.date}</p>
              </li>
            )
          })}
        </ol>
      </Layout>
    </div>
  )
}

export default BlogPage

```

## 11. Generating slugs for posts

- Tạo file `gatsby-node.js` để dựa vào Gatsby Node APIs để tạo thêm những node mới
- tại file `gatsby-node.js` :

```php
const path = require("path")
module.exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === "MarkdownRemark") {
    const slug = path.basename(node.fileAbsolutePath, ".md")

    // Tạo thêm node mới
    createNodeField({
      node,
      name: "tuandz",
      value: slug,
    })
  }
}

```

- tại `http://localhost:8000/___graphql`

```php
query{
  allMarkdownRemark{
    edges{
      node{
        fields{
          tuandz    // đây là node mới được tạo
        }
      }
    }
  }
}
```

- Kết Quả :

```php
{
  "data": {
    "allMarkdownRemark": {
      "edges": [
        {
          "node": {
            "fields": {
              "tuandz": "gatsby"    // node mới được tạo
            }
          }
        },
        {
          "node": {
            "fields": {
              "tuandz": "react"
            }
          }
        }
      ]
    }
  },
  "extensions": {}
}
```

## 12. Dynamically generating pages

- tạo pages bằng `createPages` của `Gatsby Node API`
- mục đích là để tạo ra 1 pages mới khi click vào link đến 1 bog
- Ví dụ , khi click vào 1 bog có slug là `react` thì URL sẽ là `/blog/react`

- tại file `gatsby-config.js` :

```php
module.exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const blogTemplate = path.resolve("./src/templates/blog.js")
  const res = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `)

  console.log({ res })

  res.data.allMarkdownRemark.edges.forEach(edge => {
    createPage({
      component: blogTemplate,
      path: `/blog/${edge.node.fields.slug}`,
      context: {
        slug: edge.node.fields.slug,
      },
    })
  })
}

```

- tại `pages/blog.js`

```php
import React from "react"
import Layout from "../components/layout"
import { graphql, Link, useStaticQuery } from "gatsby"

function BlogPage() {
  window.document.title = "Blog"

  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              date
            }
            fields {
              slug       ==> đây là node được tạo
            }
          }
        }
      }
    }
  `)

  console.log({ data })
  return (
    <div className="blog-page">
      <Layout>
        {" "}
        <h1>Blog page</h1>
        <ol>
          {data.allMarkdownRemark.edges.map((edge, index) => {
            return (
              <li key={index}>
                <Link to={`/blog/${edge.node.fields.slug}`}>  // chuển đến trang mới
                  {" "}
                  <h2>{edge.node.frontmatter.title}</h2>
                  <p>{edge.node.frontmatter.date}</p>
                </Link>
              </li>
            )
          })}
        </ol>
      </Layout>
    </div>
  )
}

export default BlogPage

```

## 13. Redering post data in blog template

- Mục đích là click vào bài post thì sẽ dẫn đến trang có bài viết tương ứng

```php
import React from "react"
import Layout from "../components/layout"
import { graphql } from "gatsby"

// lấy data sử dụng grapql
export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        title
        date
      }
      html
    }
  }
`

function Blog(props) {
  console.log(props)

  // Hiển thị bài viết tương ứng tại đây
  return (
    <Layout>
      <h1>{props.data.markdownRemark.frontmatter.title}</h1>  // lấy ra title của bài post
      <p>{props.data.markdownRemark.frontmatter.date}</p>  // lấy ra ngày tạo
      <div
        dangerouslySetInnerHTML={{ __html: props.data.markdownRemark.html }}   // lấy ra HTML , viết dưới dạng markdown
      ></div>
    </Layout>
  )
}

export default Blog

```

- Như vậy là khi click vào 1 bài viết thì sẽ chuyển trang sang trang có chứa nội dung tương ứng

## 14. Adding images to Posts

- Mục đích là hiển thị ảnh từ file markdown ra bài viết
- tại file `gatsby.md`

```php
---
title: "The Greate Gatsby BootCamp"
date: "05/05/2020"
---

I just launched a new bootcamp !
![Love](./love.jpg)     // đây là cú pháp hiển thị ảnh trong markdown
## Topics Covered

1. Gatsby
2. GrapQL
3. React
```

- trong gatsby để hiển thị được ảnh, và đẹp thì add thêm các plugin sau :

1. `gatsby-plugin-sharp`
2. `gatsby-remark-images`
3. `gatsby-remark-relative-images`

> Như vậy là có thể hiển thị được ảnh ra bài viết, xem kĩ hơn trong phần source code
